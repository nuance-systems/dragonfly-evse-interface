/*
    Copyright 2022-2023 Eric Poulsen

    Permission is hereby granted, free of charge, to any person obtaining 
    a copy of this software and associated documentation files (the 
    "Software"), to deal in the Software without restriction, including 
    without limitation the rights to use, copy, modify, merge, publish, 
    distribute, sublicense, and/or sell copies of the Software, and 
    to permit persons to whom the Software is furnished to do so, 
    subject to the following conditions:

    The above copyright notice and this permission notice shall be 
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
    ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
    OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#pragma once
#include <stdint.h>

typedef void (*hal_uart_recv_cb_t)(uint8_t c);

typedef enum {
    HAL_ON = 1,
    HAL_OFF = 0,
    HAL_EQ = 1,
    HAL_NEQ = 0
} hal_state_t;

typedef uint16_t hal_time_t;
#define HAL_TIME_T_ROLLOVER 0x10000

typedef enum {
    HAL_INIT_STATUS_OK              = 0,
    HAL_INIT_STATUS_BOR             = 1,
    HAL_INIT_STATUS_WDR             = 2
} hal_init_status_t;

typedef enum {
    HAL_PROX_STATE_DISCONNECTED     = 0,
    HAL_PROX_STATE_UNLATCHED        = 1,
    HAL_PROX_STATE_LATCHED          = 2
} hal_prox_state_t;

hal_init_status_t hal_init(void);

/* === INPUT === */
void hal_get_pwm_capt(uint16_t *width, uint16_t *cycle);
hal_prox_state_t hal_get_prox_state(void);

/* === OUTPUT === */

void hal_set_led(uint8_t state);
void hal_set_chg(uint8_t state);
void hal_set_ready(uint8_t state);

void hal_uart_set_recv_cb(hal_uart_recv_cb_t cb);
void hal_uart_set_baud_rate(uint32_t baudrate);
void hal_uart_send_str(const char *s);
void hal_uart_send_byte(uint8_t c);

/* === TIMING === */

hal_time_t hal_get_ms(void);
hal_time_t hal_get_ms_elapsed(hal_time_t t);

void hal_delay_ms(unsigned int ms); // Watchdog safe

/* === POWER === */

void hal_sleep(void);

/* === WATCHDOG === */

void hal_watchdog_enable(int enable);
void hal_watchdog_feed(void);


