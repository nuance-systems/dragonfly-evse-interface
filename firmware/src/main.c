/*
    Copyright 2022-2023 Eric Poulsen

    Permission is hereby granted, free of charge, to any person obtaining 
    a copy of this software and associated documentation files (the 
    "Software"), to deal in the Software without restriction, including 
    without limitation the rights to use, copy, modify, merge, publish, 
    distribute, sublicense, and/or sell copies of the Software, and 
    to permit persons to whom the Software is furnished to do so, 
    subject to the following conditions:

    The above copyright notice and this permission notice shall be 
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
    ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
    OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdint.h>
#include <ctype.h>
#include "hal.h"
#include "config.h"
#include "common.h"

typedef enum {
    STATE_IDLE = 0,
    STATE_LATCH_DELAY = 1,
    STATE_CHARGING = 2,
    STATE_FAULT = 3,
} state_t;

const char *state_names[] = {
    "idle",
    "latch_delay",
    "charging",
    "fault"
};

const char *prox_names[] = {
    "disconnected",
    "unlatched",
    "latched"
};

// These integer values are also 
// the number of blinks in a fault condition
typedef enum {
    // Recoverable
    // Unrecoverable
    FAULT_REASON_WDR = 4,
    FAULT_REASON_BOR = 5
} fault_reason_t;

static state_t state;
static hal_time_t state_time;

static fault_reason_t fault_reason;

static hal_prox_state_t prox_state;
static uint8_t pilot_pct;

static hal_time_t last_status_time;
static hal_time_t last_fault_blink_time = 0;
static hal_time_t last_pilot_sample_time = 0;

#define IS_RECOVERABLE_FAULT(x) (x < FAULT_REASON_WDR)

#define ENTER_FAULT_STATE(reason) do {state = STATE_FAULT; fault_reason = reason;} while(0)

volatile int uart_data_byte = -1;
static void uart_rx_cb(uint8_t c) {
    uart_data_byte = c;
}

static void send_status(hal_prox_state_t prox_state, uint8_t pilot_pct) {
    hal_uart_send_str("---\n");
    hal_uart_send_str("state: ");
    hal_uart_send_str(state_names[state]);
    hal_uart_send_byte('\n');
    hal_uart_send_str("prox: ");
    hal_uart_send_str(prox_names[prox_state]);
    hal_uart_send_byte('\n');
    hal_uart_send_str("pilot_pct: 0x");
    write_hex_number(pilot_pct);
    hal_uart_send_byte('\n');
    last_status_time = hal_get_ms();
}

static void enter_state(state_t _state) {
    static state_t state_last = -1;
    static hal_prox_state_t prox_state_last = -1;

    state = _state;
    state_time = hal_get_ms();
    if (state_last != state || prox_state_last != prox_state) {
        state_last = state;
        prox_state_last = prox_state;
        send_status(prox_state, pilot_pct);
    }
}

static uint8_t get_pilot_pct(void) {
    uint16_t width, cycle;
    hal_get_pwm_capt(&width, &cycle);
    return (width && cycle) ? (uint32_t)width * 100 / cycle : 0;
}

int main() {
    enter_state(STATE_IDLE);

    switch (hal_init()) {
        case HAL_INIT_STATUS_WDR:
            ENTER_FAULT_STATE(FAULT_REASON_WDR);
            break;

        case HAL_INIT_STATUS_BOR:
            ENTER_FAULT_STATE(FAULT_REASON_BOR);
            break;

        case HAL_INIT_STATUS_OK:
            break;
    }

    //hal_watchdog_enable(1);

    for (int i = 3; i; i--) {
        hal_set_led(1);
        hal_delay_ms(100);
        hal_set_led(0);
        hal_delay_ms(100);
    }
    hal_uart_set_recv_cb(uart_rx_cb);

    last_status_time = hal_get_ms();

    while (1) {
        hal_watchdog_feed();
        prox_state = hal_get_prox_state();
        if (hal_get_ms_elapsed(last_pilot_sample_time) > 100) {
            last_pilot_sample_time = hal_get_ms();
            pilot_pct = get_pilot_pct();
        }

        switch(state) {
            case STATE_IDLE: 
                if(prox_state == HAL_PROX_STATE_LATCHED) {
                    enter_state(STATE_LATCH_DELAY); 
                } else if (pilot_pct) {
                    // remain idle as long as pilot is detected
                    enter_state(STATE_IDLE);
                } else if (hal_get_ms_elapsed(state_time) > CONFIG_IDLE_BEFORE_SLEEP_TIME_MS) {
                    send_status(prox_state, pilot_pct);
                    hal_sleep();
                    enter_state(STATE_IDLE); // resets state time 
                }
                break;

            case STATE_LATCH_DELAY:
                if (prox_state != HAL_PROX_STATE_LATCHED) {
                    enter_state(STATE_IDLE);
                } else if (hal_get_ms_elapsed(state_time) > CONFIG_LATCH_SETTLE_BEFORE_READY_TIME_MS) {
                    hal_set_ready(1);
                    hal_set_chg(1);
                    hal_set_led(1);
                    enter_state(STATE_CHARGING);
                }
                break;

            case STATE_CHARGING:
                if (prox_state != HAL_PROX_STATE_LATCHED) {
                    hal_set_ready(0);
                    hal_set_chg(0);
                    hal_set_led(0);
                    enter_state(STATE_IDLE);
                }
            break;

            case STATE_FAULT:
                if (hal_get_ms_elapsed(last_fault_blink_time)> CONFIG_FAULT_BLINK_GAP_TIME_MS){
                    last_fault_blink_time = hal_get_ms();
                    for(int i = 0; i < fault_reason; ++i) {
                        hal_set_led(1);
                        hal_delay_ms(CONFIG_FAULT_BLINK_ON_TIME_MS);
                        hal_set_led(0);
                        hal_delay_ms(CONFIG_FAULT_BLINK_OFF_TIME_MS);
                    }
                }
            break;
        }

        if (hal_get_ms_elapsed(last_status_time) > 5000) {
            send_status(prox_state, pilot_pct);
        }
    }
}

