/*
    Copyright 2022-2023 Eric Poulsen

    Permission is hereby granted, free of charge, to any person obtaining 
    a copy of this software and associated documentation files (the 
    "Software"), to deal in the Software without restriction, including 
    without limitation the rights to use, copy, modify, merge, publish, 
    distribute, sublicense, and/or sell copies of the Software, and 
    to permit persons to whom the Software is furnished to do so, 
    subject to the following conditions:

    The above copyright notice and this permission notice shall be 
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
    ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
    OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#define __DELAY_BACKWARD_COMPATIBLE__
#include "hal.h"
#include "common.h"
#include <stddef.h>
#include <string.h>
#undef F_CPU
#define F_CPU 20000000
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <avr/sleep.h>
#include <util/delay.h>

#define USE_PRECHARGE_BOARD 1

// PORTA
#define HW_VER 20

#if HW_VER == 10

#define PROX_ENABLE_PORT    A
#define PROX_ENABLE_PIN     2

#define READY_PORT          A
#define READY_PIN           3

#define LED_PORT            A
#define LED_PIN             4

#define CHG_PORT            A
#define CHG_PIN             5

#define PILOT_PORT          A
#define PILOT_PIN           6

#define VAC_PORT            B
#define VAC_PIN             0

#define PROX_PORT           B
#define PROX_PIN            1
#define PROX_AIN            ADC_MUXPOS_AIN10_gc

#define UART_TX_PORT        B
#define UART_TX_PIN         2

#define UART_RX_PORT        B
#define UART_RX_PIN         3

#elif HW_VER == 11

#define CHG_PORT            A
#define CHG_PIN             1

#define LED_PORT            A
#define LED_PIN             2

#define PROX_ENABLE_PORT    A
#define PROX_ENABLE_PIN     4

#define PROX_PORT           A
#define PROX_PIN            5
#define PROX_AIN            ADC_MUXPOS_AIN5_gc

#define READY_PORT          A
#define READY_PIN           6

#define PILOT_PORT          A
#define PILOT_PIN           7

#define STBY_PORT           B
#define STBY_PIN            0

#define UART_TX_PORT        B
#define UART_TX_PIN         2

#define UART_RX_PORT        B
#define UART_RX_PIN         3

#define VAC_PORT            B 
#define VAC_PIN             4

#define INT_PORT            B
#define INT_PIN             5
#elif HW_VER == 20

#define CHG_PORT            A
#define CHG_PIN             1

#define LED_PORT            A
#define LED_PIN             2

#define PROX_ENABLE_PORT    A
#define PROX_ENABLE_PIN     4

#define PROX_PORT           A
#define PROX_PIN            5
#define PROX_AIN            ADC_MUXPOS_AIN5_gc

#define READY_PORT          A
#define READY_PIN           6

#define PILOT_PORT          A
#define PILOT_PIN           7

#define UART_TX_PORT        B
#define UART_TX_PIN         2

#define UART_RX_PORT        B
#define UART_RX_PIN         3

#define VAC_PORT            B 
#define VAC_PIN             4

#else
#error Undefined HW_VER
#endif


#define OUTPUT 1
#define INPUT 0
#define _CONCAT2_(y,z) y ## z
#define _CONCAT3_(x,y,z) x ## y ## z
#define _CONCAT5_(v,w,x,y,z) v ## w ## x ## y ## z
#define _CONCAT7_(t,u,v,w,x,y,z) t ## u ## v ## w ## x ## y ## z
#define PIN_DIR(port, pin, dir) do { \
    if (dir) { \
        _CONCAT3_(PORT, port, _DIRSET) = (1 << pin); \
    } else { \
        _CONCAT3_(PORT, port, _DIRCLR) = (1 << pin); \
    } \
} while(0)

#define PIN_WRITE(port, pin, value) do { \
    if (value) { \
        _CONCAT3_(PORT, port, _OUTSET) = (1 << pin); \
    } else { \
        _CONCAT3_(PORT, port, _OUTCLR) = (1 << pin); \
    } \
} while(0)

#define PIN_READ(port, pin) (!!(_CONCAT3_(PORT, port, _IN) & (1 << pin)))

#define PIN_PULLUP(port, pin, pullup) do { \
    if (pullup) { \
        _CONCAT5_(PORT, port, _PIN, pin, CTRL) |= PORT_PULLUPEN_bm; \
    } else { \
        _CONCAT5_(PORT, port, _PIN, pin, CTRL) &= ~PORT_PULLUPEN_bm; \
    } \
} while(0)

#define PIN_INT(port, pin, int_type)  do { \
    _CONCAT5_(PORT, port, _PIN, pin, CTRL) = \
    (_CONCAT5_(PORT, port, _PIN, pin, CTRL) & (~PORT_ISC_gm)) | (int_type & PORT_ISC_gm); \
} while(0)

#define UART_TX_BUSY() (uart_tx_buf_len || !(USART0.STATUS & USART_DREIF_bm))

#define ASYNC_PIN_EVENT(chan, port, pin) _CONCAT7_(EVSYS_ASYNCCH, chan, _PORT, port, _PIN, pin, _gc)

inline static void clock_setup(void) {
    CPU_CCP = CCP_IOREG_gc;                 // unlock CLKCTRL.MCLKCTRLB
    CLKCTRL.MCLKCTRLB &= ~CLKCTRL_PEN_bm;   // Disable main clk prescaler
}

static volatile hal_time_t ticks;


inline static void timer_setup(void) {
    // SETUP TCA
    // for 1ms interrupts
    // system ticks
    TCA0.SINGLE.PER = F_CPU / 1000; // 1 ms at 20MHz
    TCA0.SINGLE.CTRLA = TCA_SINGLE_ENABLE_bm;
    TCA0.SINGLE.INTCTRL |= TCA_SINGLE_OVF_bm;


    // Setup TCB
    // For freq & pw measurement
    TCB0.CTRLB = TCB_CNTMODE_FRQPW_gc;
    //TCB0.CTRLB = TCB_CNTMODE_PW_gc;
    TCB0.EVCTRL = TCB_CAPTEI_bm;
    TCB0.INTCTRL = TCB_CAPT_bm;
    TCB0.CTRLA = TCB_ENABLE_bm;
}

static void events_setup(void) {
    EVSYS.ASYNCCH0 = ASYNC_PIN_EVENT(0, PILOT_PORT, PILOT_PIN);
    EVSYS.ASYNCUSER0 = EVSYS_ASYNCUSER0_ASYNCCH0_gc;
}

ISR (TCA0_OVF_vect) {
    TCA0.SINGLE.INTFLAGS = TCA_SINGLE_OVF_bm;
    ++ticks;
}

static volatile uint16_t cap_cycle_cnt;
static volatile uint16_t cap_high_cnt;
static volatile uint8_t cap_flag;

ISR (TCB0_INT_vect) {
    cap_flag = 1;
    cap_cycle_cnt = TCB0.CNT;
    cap_high_cnt = TCB0.CCMP;
}


#define BAUDRATE 57600
#define CLK_PER F_CPU


// UART handling

static volatile const uint8_t *uart_tx_buf;
static volatile uint8_t uart_tx_buf_len;
static hal_uart_recv_cb_t uart_rx_cb = NULL;

ISR (USART0_RXC_vect) {
    USART0.STATUS |= USART_RXSIF_bm;
    uart_rx_cb(USART0.RXDATAL);
}

ISR (USART0_DRE_vect) {

    USART0.TXDATAL = *uart_tx_buf++;
    if(!--uart_tx_buf_len) {
        USART0.CTRLA &= ~(USART_DREIE_bm);
    }
}
ISR (USART0_TXC_vect) {
    USART0.STATUS |= USART_TXCIF_bm;
}

static void uart_setup(void) {
    hal_uart_set_baud_rate(BAUDRATE);

    // Async, 8N1
    USART0.CTRLC =  USART_CMODE_ASYNCHRONOUS_gc |
                    USART_CHSIZE_8BIT_gc | 
                    USART_PMODE_DISABLED_gc |
                    USART_SBMODE_1BIT_gc;
            
    // enable RX
    // enable TX
    // normal RX mode (async)
    USART0.CTRLB =  USART_RXEN_bm |
                    USART_TXEN_bm |
                    USART_RXMODE_NORMAL_gc;

}

#if 0
static inline void uart_enable(uint8_t enable) {
    if (enable) {
        USART0.CTRLB |= USART_RXEN_bm | USART_TXEN_bm;
    } else {
        USART0.CTRLB &= ~(USART_RXEN_bm | USART_TXEN_bm);
    }
}
#endif

inline static void gpio_setup(void) {

    PIN_DIR(UART_TX_PORT, UART_TX_PIN, OUTPUT);
    PIN_DIR(UART_RX_PORT, UART_RX_PIN, INPUT);

    // PROX_ENABLE
    PIN_DIR(PROX_ENABLE_PORT, PROX_ENABLE_PIN, OUTPUT);
    PIN_WRITE(PROX_ENABLE_PORT, PROX_ENABLE_PIN, 1);

    // READY
    PIN_WRITE(READY_PORT, READY_PIN, 0);
    PIN_DIR(READY_PORT, READY_PIN, OUTPUT);

    // LED
    PIN_WRITE(LED_PORT, LED_PIN, 0);
    PIN_DIR(LED_PORT, LED_PIN, OUTPUT);

    // CHG
    PIN_WRITE(CHG_PORT, CHG_PIN, 0);
    PIN_DIR(CHG_PORT, CHG_PIN, OUTPUT);

    // PILOT
    PIN_DIR(PILOT_PORT, PILOT_PIN, INPUT);

    // VAC
    PIN_DIR(VAC_PORT, VAC_PIN, INPUT);

    // PROX
    PIN_DIR(PROX_PORT, PROX_PIN, INPUT);
}

ISR (PORTA_PORT_vect) {
    PORTA.INTFLAGS = PORTA.INTFLAGS; // clear interrupts
}

ISR (PORTB_PORT_vect) {
    PORTB.INTFLAGS = PORTB.INTFLAGS; // clear interrupts
}

inline static void adc_pause() {
    ADC0.CTRLA &= ~(ADC_ENABLE_bm);
}

inline static void adc_resume() {
    ADC0.CTRLA |= ADC_ENABLE_bm;
    ADC0.COMMAND = ADC_STCONV_bm;
}

inline static void adc_setup() {
    // VDD reference, HV cap
    // For some reason, the ADC is always max value when using
    // a prescaler less than 8
    ADC0.CTRLC = ADC_REFSEL_VDDREF_gc | ADC_SAMPCAP_bm | ADC_PRESC_DIV8_gc; 

    // Select prox input
    ADC0.MUXPOS = PROX_AIN;

    // enable ADC, 8 bit resolution
    ADC0.CTRLA = ADC_RESSEL_8BIT_gc | ADC_FREERUN_bm;
    adc_resume();
}



//
/// PUBLIC
//

hal_init_status_t hal_init(void) {
    hal_watchdog_enable(0);
    clock_setup();
    gpio_setup();
    uart_setup();
    timer_setup();
    events_setup();
    adc_setup();
    sei();
    if (RSTCTRL.RSTFR & RSTCTRL_WDRF_bm) {
        return HAL_INIT_STATUS_WDR; 
    } else if (RSTCTRL.RSTFR & RSTCTRL_BORF_bm) {
        return HAL_INIT_STATUS_BOR;
    }
    return HAL_INIT_STATUS_OK;
}


/* === INPUT === */

void hal_get_pwm_capt(uint16_t *width, uint16_t *cycle) {
    cli();
    if (cap_flag) {
        *width = cap_high_cnt;
        *cycle = cap_cycle_cnt;
        cap_flag = 0;
    } else {
        *width = *cycle = 0;
    }
    sei();
}

/*
 * Disconnected
 *     R: 330 : 2700   0.89
 *     V: 4.45
 *   ADC: ~227 (8 bit) 0xE3
 * 
 * Unlatched
 *     R: 330 : 408   0.55
 *     V: 2.76
 *   ADC: ~140 0x8C
 * 
 * Latched
 *     R: 330 : 142   0.3
 *     V: 1.5
 *   ADC: ~77 0x4D
 */

hal_prox_state_t hal_get_prox_state(void) {
    uint8_t adc_val = ADC0.RES;
    if (adc_val > 183) {
        return HAL_PROX_STATE_DISCONNECTED;
    } else if (adc_val > 108) {
        return HAL_PROX_STATE_UNLATCHED;
    }
    return HAL_PROX_STATE_LATCHED;
}

/* === OUTPUT === */

void hal_set_led(uint8_t state) {
    PIN_WRITE(LED_PORT, LED_PIN, !!state);
}

void hal_set_chg(uint8_t state) {
    PIN_WRITE(CHG_PORT, CHG_PIN, !!state);
}

void hal_set_ready(uint8_t state) {
    PIN_WRITE(READY_PORT, READY_PIN, !!state);
}

void hal_uart_set_baud_rate(uint32_t baudrate) {
    USART0.BAUD = (64 * CLK_PER) / (16 * baudrate);
}

void hal_uart_send_data(const uint8_t* data, uint8_t len) {
    while(USART0.CTRLA & USART_DREIE_bm) { ; } // wait for any cur tx
    if (data && len) {
        cli();
        uart_tx_buf = data;
        uart_tx_buf_len = len;
        USART0.CTRLA |= USART_DREIE_bm;
        sei();
    }
}

void hal_uart_send_byte(uint8_t c) {
    //static uint8_t x;
    //x = c;
    //hal_uart_send_data(&x, 1);
    while(UART_TX_BUSY()) { ; } // wait for any cur tx
    USART0.TXDATAL = c;
}

void hal_uart_send_str(const char *s) {
    hal_uart_send_data((const uint8_t*)s, strlen(s));
}

void hal_uart_set_recv_cb(hal_uart_recv_cb_t cb) {
    cli();
    uart_rx_cb = cb;
    if (cb) {
        USART0.CTRLA |= USART_RXCIE_bm;
    } else {
        USART0.CTRLA &= ~(USART_RXCIE_bm);
    }
    sei();
}


/* === TIMING === */

// WDT is 16ms timeout
#define DELAY_CHUNK_MS 10

void hal_delay_ms(hal_time_t ms) {
    for(unsigned int i = 0; i < (ms / DELAY_CHUNK_MS); ++i) {
        _delay_ms(DELAY_CHUNK_MS);
        hal_watchdog_feed();
    }
    _delay_ms(ms % DELAY_CHUNK_MS);
    hal_watchdog_feed();
}

hal_time_t hal_get_ms(void) {
    hal_time_t now;
    cli(); // atomically get two-byte value
    now = ticks;
    sei();
    return now;
}

hal_time_t hal_get_ms_elapsed(hal_time_t t) {
    uint32_t now;
    cli(); // atomically get two-byte value
    now = ticks;
    sei();
    if (now < t) {
        now += HAL_TIME_T_ROLLOVER;
    }
    return now - t;
}

/* === POWER === */
void hal_sleep(void) {

    PIN_INT(PILOT_PORT, PILOT_PIN, PORT_ISC_BOTHEDGES_gc);

    // Turn off proximity pulldown resistor 
    // to lower quiescent current
    PIN_WRITE(PROX_ENABLE_PORT, PROX_ENABLE_PIN, 0);

    while(UART_TX_BUSY()) { ; }
    hal_delay_ms(2); //ensure all data is sent
    USART0.CTRLB |= USART_SFDEN_bm; // enable start bit detect
    USART0.CTRLA |= USART_RXSIE_bm; // 

    adc_pause();

    set_sleep_mode(SLEEP_MODE_STANDBY);
    sleep_mode();

    adc_resume();

    PIN_WRITE(PROX_ENABLE_PORT, PROX_ENABLE_PIN, 1);
    PIN_INT(PILOT_PORT, PILOT_PIN, PORT_ISC_INTDISABLE_gc);
}

/* === WATCHDOG === */

void hal_watchdog_enable(int enable) {
    cli();
    wdt_reset(); // seems to be required to enable WDT
    _delay_ms(20); // inexplicably necessary
    if (enable) {
        CPU_CCP = CCP_IOREG_gc; // unlock WDG.CTRLA
        WDT.CTRLA = PERIOD_16CLK_gc;
    } else {
        CPU_CCP = CCP_IOREG_gc; // unlock WDG.CTRLA
        WDT.CTRLA = 0;
    }
    sei();
}

void hal_watchdog_feed(void) {
    wdt_reset();
}