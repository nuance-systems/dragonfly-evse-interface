# Dragonfly Precharge Firmware


## Build Requirements

### Make

Ensure `make` is installed.

### Platform IO + pymcuprog

* Install [Platformio Core](https://docs.platformio.org/en/stable/core/index.html)
* Install [pymcuprog](https://pypi.org/project/pymcuprog/)

## Flashing the firmware

Flashing is via the UPDI pin on the attiny404 IC.  The programmer is the a `serialupdi` type, and a simple UART based UPDI programmer can be built using a USB-->TTL UART adapter (e.g. the ubiquitous FTDI232RL adapters), with a 1K resistor connected between TX and RX

                            Vcc                     Vcc
                            +-+                     +-+
                            |                       |
    +---------------------+ |                       | +--------------------+
    | Serial port         +-+                       +-+  AVR device        |
    |                     |      +----------+         |                    |
    |                  TX +------+   1k     +---------+ UPDI               |
    |                     |      +----------+    |    |                    |
    |                     |                      |    |                    |
    |                  RX +----------------------+    |                    |
    |                     |                           |                    |
    |                     +--+                     +--+                    |
    +---------------------+  |                     |  +--------------------+
                            +-+                   +-+
                            GND                   GND

To build and flash the firmware (once the programmer is connected to the board):

`make upload`

