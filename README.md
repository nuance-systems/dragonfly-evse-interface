# Dragonly EVSE Interface
![PCB Render](img/evse_board.png)
![PCB Render](img/evse_board_back.png)

The Dragonfly EVSE Interface is designed to be installed in an electric vehicle as an interface for the [J1772](https://en.wikipedia.org/wiki/SAE_J1772) EVSE (also known as an "EV Charger").

When an EVSE is connected, it will direct the EVSE to begin sending power into the EV, and has an output signal to inform the onboard EV charge circuitry.

## Wiring:

### For the EVSE

* `12V`: Input: The positive terminal of your vehicles 12V system.  The onboard regulator has a max input voltage of 45V, but it is not recommended to exceed 24V.
* `GND`: Input: Connect this to the PE from your J1772 port.  This should also be the common ground for your vehicles 12V system.
* `CHG`: Output: This is an open drain output capable of sinking about 1A.  The MOSFET is rated for an absolute maximum of 20V; it is not recommended to exceed 15V.  This can be used to indicated to the EV systems that the EVSE is providing charging power.
* `PILOT`: Input: This should be connected to the J1772 "CP" (Control Pilot) signal.
* `PROX`: Input: This should be connected to the J1772 "PP" (Proximity Pilot) signal.

### For the J1772 port

PE, PP, and CP signals should be connected to the EVSE interface, per above.

The PE, L1, and L2/N signals should be connected to your onboard battery charger.

## Hardware

Please refer to the [hardware readme](hardware/README.md) for details.

## Firmware

Please refer to the [firmware readme](firmware/README.md) for details.

## Other considerations

The device should consume &lt;1mA (typically ~500µA) while idle.  