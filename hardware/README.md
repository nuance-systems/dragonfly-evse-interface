
# Schematic

![schematic](../img/schematic.png)
[PDF](../img/schematic.pdf)


## Changelog

* 2.1 Fixed HW bug with EN pin of U1 (vreg) not being pulled up
* 2.0 Removed UART, VAC, CAN, etc.
* 1.0 Initial version with VAC input and UART TX/RX; a bit overcomplicated